/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function userInfo() {

	let fullName = prompt ("Enter your Full Name");
	let age = prompt ("Enter your Age");
	let location = prompt ("Enter your Location");

	console.log(fullName + " " + age + " " + location);
	
}
	userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function bandList() {

		let band1 = "Parokya ni Edgar";
		let band2 = "Eraserheads";
		let band3 = "Kamikazee";
		let band4 = "Spongcola";
		let band5 = "Hale";

		console.log (band1);
		console.log (band2);
		console.log (band3);
		console.log (band4);
		console.log (band5);
	}

	bandList();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function showFavMovies() {

		let movie1 = "Avengers: Endgame - RT rating is 94%";
		let movie2 = "Avengers: Infinity War - RT rating is 85%";
		let movie3 = "Avengers: Age of Ultron - RT rating is 76%";
		let movie4 = "The Avengers - RT rating is 91%";
		let movie5 = "Iron Man 3- RT rating is 79%";

		console.log (movie1);
		console.log (movie2);
		console.log (movie3);
		console.log (movie4);
		console.log (movie5);
	}

	showFavMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

/*
console.log(friend1);
console.log(friend2);*/

printFriends();